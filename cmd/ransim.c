/*
This program is free software: you can redistribute 
it and/or modify it under the terms of the GNU General 
Public License as published by the Free Software Foundation, 
either version 3 of the License, or (at your option) any 
later version.

This program is distributed in the hope that it will be 
useful, but WITHOUT ANY WARRANTY; without even the implied 
warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR 
PURPOSE. See the GNU General Public License for more 
details.
*/

#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <err.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdint.h>
#include <sys/random.h>
#include <dirent.h>

/*
	See CryptoLock paper (Scaife et al., 2016)

	Class A: open / read / write / close
	Class B: move / encrypt / move
	Class C: create / encrypt / replace
*/

enum{
	ClassA,
	ClassB,
	ClassC,
	Nclasses,
	Maxpath = 8*1024,
	Bufsz = 8*1024,
};

char *classes[Nclasses]={
	"class-a",
	"class-b",
	"class-c",
};

static void
usage(void)
{
	fprintf(stderr, "usage: ransim [class-a | class-b | class-c] dir\n");
	exit(EXIT_FAILURE);
}

static void
fakecipher(char *buf, int sz)
{
	getrandom(buf, sz, GRND_NONBLOCK);
}

static void
open_enc_close(char *path)
{
	int fd;
	char buf[Bufsz];
	int nr;

	fd = open(path, O_RDWR);
	if(fd < 0){
		err(EXIT_FAILURE, "can't open file: %s", path);
	}
	while((nr = read(fd, buf, Bufsz)) > 0){
		fakecipher(buf, nr);
		lseek(fd, -nr, SEEK_CUR);
		if(write(fd, buf, nr) != nr){
			err(EXIT_FAILURE, "can't write");
		}
	}
	if(nr < 0){
		err(EXIT_FAILURE, "can't read");
	}
	close(fd);
}

static void
move_enc_move(char *path)
{
	if(rename(path, "/tmp/ransim") < 0){
		err(EXIT_FAILURE, "can't move file: %s", path);
	}
	open_enc_close("/tmp/ransim");
	if(rename("/tmp/ransim", path) < 0){
		err(EXIT_FAILURE, "can't move file: %s", path);
	}
}

static void
create_enc_move(char *path)
{
	int fd;
	int fdnew;
	char buf[Bufsz];
	int nr;

	fd = open(path, O_RDONLY);
	if(fd < 0){
		err(EXIT_FAILURE, "can't open file: %s", path);
	}
	fdnew = open("/tmp/ransim", O_TRUNC|O_CREAT|O_WRONLY, 0600);
	if(fdnew < 0){
		err(EXIT_FAILURE, "can't open file: %s", path);
	}
	while((nr = read(fd, buf, Bufsz)) > 0){
		fakecipher(buf, nr);
		if(write(fdnew, buf, nr) != nr){
			err(EXIT_FAILURE, "can't write");
		}
	}
	if(nr < 0){
		err(EXIT_FAILURE, "can't read");
	}
	close(fd);
	close(fdnew);
	if(rename("/tmp/ransim", path) < 0){
		err(EXIT_FAILURE, "can't move file: %s", path);
	}
}

static void
simencrypt(char *path, int class)
{
	switch(class){
	case ClassA:
		open_enc_close(path);
		break;
	case ClassB:
		move_enc_move(path);
		break;
	case ClassC:
		create_enc_move(path);
		break;
	default:
		errx(EXIT_FAILURE, "no such class: %d", class);
	}
}

static void
tree(char *path, int class)
{
	DIR *d;
	struct dirent *ent;
	char nextp[Maxpath];

	d = opendir(path);
	if(d == NULL){
		err(EXIT_FAILURE, "opendir failed: %s", path);
	}
	while((ent = readdir(d)) != NULL){
		if(strcmp(ent->d_name, ".") == 0 ||
				strcmp(ent->d_name, "..") == 0){
			continue;
		}
		snprintf(nextp, Maxpath, "%s/%s", path, ent->d_name);
		if(ent->d_type == DT_DIR){
			tree(nextp, class);
		}else if(ent->d_type == DT_REG){
			simencrypt(nextp, class);
		}
	}
	closedir(d);
}

int
main(int argc, char *argv[])
{
	int class;
	int i;
	char resp[8];

	if(argc != 3){
		usage();
	}
	for(i=0; i<Nclasses; i++){
		if(strcmp(argv[1], classes[i]) == 0){
			class = i;
			break;
		}
	}
	if(i == Nclasses){
		usage();
	}
	printf("%s will be destroyed, proceed? (type 'yes')\n", argv[2]);
	if(fgets(resp, 8, stdin) != NULL && strncmp(resp, "yes\n", 4) == 0){
		tree(argv[2], class);
	}
	exit(EXIT_SUCCESS);
}
