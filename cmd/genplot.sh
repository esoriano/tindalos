#!/bin/sh

echo x_value,total_1,total_2 > data.csv
tail -n 0 -f /var/log/kern.log  | grep --line-buffered 'Tindalos thread: lru pages' | sed -u 's/^.*lru pages: //' | awk -W interactive '{print NR "," $6 "," $9 ; fflush()}' >> data.csv
