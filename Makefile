TINDALOS_VERSION="0.1"

EXTRA_CFLAGS += -DTINDALOS_VERSION=\"$(TINDALOS_VERSION)\"

obj-m += tindalos.o

tindalos-y := main.o

all:
	make -C /lib/modules/$(shell uname -r)/build M=$(shell pwd) modules

clean:
	make -C /lib/modules/$(shell uname -r)/build M=$(shell pwd) clean

.PHONY: all clean
